require_relative '../models/User'
require_relative '../models/Context'

class Commands
  # description: Bot command handlers

  # /start
  def self.start(bot, message)
    text = "This is a template for Ruby bot building."
    bot.api.send_message(chat_id: message.chat.id, text: text)
  end

  # /error
  def self.error(bot, message)
    bot.api.send_message(chat_id: message.chat.id, text: "Command error!")
    Context.clear_state(message.chat.id)
    help(bot, message)
  end

  # /help
  def self.help(bot, message)
    help_list = "/start - Start message\n/error - Error message\n"
    bot.api.send_message(chat_id: message.chat.id, text: help_list)
  end

  # /cancel
  def self.cancel(bot, message)
    if @context = Context.get_state(message.chat.id)
      Context.clear_state(message.chat.id)
      bot.api.send_message(chat_id: message.chat.id, text: "Canceled.")
    else
      bot.api.send_message(chat_id: message.chat.id, text: "No command session is currently set.")
    end
  end
end
